﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace DAL
{
    public static class AccountService
    {
        //remove un account de la bdd
        public static bool RemoveAccount(Account account)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "delete from account where id = @id";
                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Value = account.Id;
                    command.ExecuteNonQuery();
                }
            }

            return true;
        }

        //update un account dans la bdd
        public static bool UpdateAccount(Account account , string type)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    if(type == "email")
                    {
                        command.CommandText = "UPDATE account SET email = @eMail WHERE id = @id ";

                        command.Parameters.Add("@id", SqlDbType.Int);
                        command.Parameters["@id"].Value = account.Id;

                        command.Parameters.Add("@eMail", SqlDbType.NVarChar);
                        command.Parameters["@eMail"].Value = account.Email;
                    }
                        
                    else if(type == "pw")
                    {
                        command.CommandText = "UPDATE account SET password = @pw WHERE id = @id ";

                        command.Parameters.Add("@id", SqlDbType.Int);
                        command.Parameters["@id"].Value = account.Id;

                        UTF8Encoding utf8 = new UTF8Encoding();
                        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

                        command.Parameters.Add("@pw", SqlDbType.NVarChar);
                        command.Parameters["@pw"].Value = BitConverter.ToString(md5.ComputeHash(utf8.GetBytes(account.Password)));
                    }

                    command.ExecuteNonQuery();
                }
            }

            return true;
        }

        //add un account dans la bdd
        public static bool AddAccount(Account account)
        {
            if (!AccountValidation.UserNameIsTaken(account.Username))
            {
                SqlConnection conn;
                string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

                using (conn = new SqlConnection(myConn))
                {
                    conn.Open();

                    using (SqlCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "insert into account(username,password,first_name,last_name,email,phone,sexe) values (@username,@password,@firstName,@lastName,@eMail,@phone,@sexe)";

                        command.Parameters.Add("@username", SqlDbType.NVarChar);
                        command.Parameters["@username"].Value = account.Username;

                        UTF8Encoding utf8 = new UTF8Encoding();
                        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

                        command.Parameters.Add("@password", SqlDbType.NVarChar);
                        command.Parameters["@password"].Value = BitConverter.ToString(md5.ComputeHash(utf8.GetBytes(account.Password)));

                        command.Parameters.Add("@firstName", SqlDbType.NVarChar);
                        command.Parameters["@firstName"].Value = account.FirstName.Substring(0, 1).ToUpper() + account.FirstName.Substring(1);

                        command.Parameters.Add("@lastName", SqlDbType.NVarChar);
                        command.Parameters["@lastName"].Value = account.LastName.Substring(0, 1).ToUpper() + account.LastName.Substring(1);

                        command.Parameters.Add("@eMail", SqlDbType.NVarChar);
                        command.Parameters["@eMail"].Value = account.Email;

                        string firstPhonePart = account.Phone.Substring(0, 3);
                        string secondPhonePart = account.Phone.Substring(3, 3);
                        string thirdPhonePart = account.Phone.Substring(6, 4);
                        string phone = firstPhonePart + "-" + secondPhonePart + "-" + thirdPhonePart;

                        command.Parameters.Add("@phone", SqlDbType.NVarChar);
                        command.Parameters["@phone"].Value = phone;

                        string sexe = null;

                        if (account.Sexe == true)
                            sexe = "Male";
                        else if (account.Sexe == false)
                            sexe = "Female";
                        else
                            sexe = "Unknown";

                        command.Parameters.Add("@sexe", SqlDbType.NVarChar);
                        command.Parameters["@sexe"].Value = sexe;

                        command.ExecuteNonQuery();
                    }
                }

                return true;
            }
            else
                return false;

        }
    }
}
