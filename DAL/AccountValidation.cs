﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class AccountValidation
    {
        //test si le username es deja prit
        public static bool UserNameIsTaken(string username)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select username from account where username = @username";
                    command.Parameters.Add("@username", SqlDbType.NVarChar);
                    command.Parameters["@username"].Value = username;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            return true;
                        else
                            return false;
                    }                 
                }
            }
        }
    }
}
