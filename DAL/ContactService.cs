﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public static class ContactService
    {

        //retourn un adress selon un string
        public static Adress StringToAdress(string adress)
        {
            adress = adress.Trim();

            if(adress.Length < 1)
                return new Adress();
            else
            {
                string[] myTab = adress.Split('/');
                return new Adress() { NoHouse = int.Parse(myTab[0]), RoadName = myTab[1], City = myTab[2], Country = myTab[3], State = myTab[4] };              
            }

        }

        //retourne un contact selon son id
        public static Contact getContactById(int id)
        {
            Contact contact;
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select * from Table_contact where id = @id";

                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Value = id;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            return contact = new Contact()
                            {
                                Id = reader.GetInt32(0),
                                FirstName = string.Format("{0}", reader[1]).Trim(),
                                LastName = string.Format("{0}", reader[2]).Trim(),
                                Clue = string.Format("{0}", reader[3]).Trim(),
                                Email = string.Format("{0}", reader[4]).Trim(),
                                Phone = string.Format("{0}", reader[5]).Trim(),
                                Adress = ContactService.StringToAdress(string.Format("{0}", reader[6]))
                            };
                        }
                    }
                }

            }

            return new Contact();
        }
        
        //retounr une liste de contact qui contient tous les contact
        public static List<Contact> getAllContact(Account accountid)
        {
            List<Contact> contactList = new List<Contact>();
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select * from Table_contact where id_user = @id";

                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Value = accountid.Id;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            contactList.Add(new Contact()
                            {
                                Id = reader.GetInt32(0),
                                FirstName = string.Format("{0}", reader[1]).Trim(),
                                LastName = string.Format("{0}", reader[2]).Trim(),
                                Clue = string.Format("{0}", reader[3]).Trim(),
                                Email = string.Format("{0}", reader[4]).Trim(),
                                Phone = string.Format("{0}", reader[5]).Trim(),
                                Adress = ContactService.StringToAdress(string.Format("{0}",reader[6]))
                            });
                        }
                    }
                }

                return contactList;
            }
        }

        //fait une recherche avancer et retourne une liste
        public static List<Contact> searchadvancedSetting(string str , searchBy sb , bool? cluenull , bool? emailnull , bool? phonenull , bool? adressnull , int iduser)
        {
            List<Contact> contactList = new List<Contact>();
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";
            string toSearch = "";
            string clueRep = "";
            string phoneRep = "";
            string adressRep = "";
            string emailRep = "";

            if (sb == searchBy.clue)
                toSearch = "clue";
            else if (sb == searchBy.FirstName)
                toSearch = "first_name";
            else if (sb == searchBy.Lastname)
                toSearch = "last_name";

            if (cluenull == true)
                clueRep = "is null ";
            else if (cluenull == false)
                clueRep = "is not null ";
            else if (cluenull == null)
                clueRep = "is null or clue is not null ";

            if (emailnull == true)
                emailRep = "is null ";
            else if (emailnull == false)
                emailRep = "is not null ";
            else if (emailnull == null)
                emailRep = "is null or email is not null ";

            if (adressnull == true)
                adressRep = "is null ";
            else if (adressnull == false)
                adressRep = "is not null ";
            else if (adressnull == null)
                adressRep = "is null or adress is not null ";

            if (phonenull == true)
                phoneRep = "is null ";
            else if (phonenull == false)
                phoneRep = "is not null ";
            else if (phonenull == null)
                phoneRep = "is null or phone is not null ";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    if(sb == searchBy.clue)
                        command.CommandText = "select * from Table_contact where " + toSearch + " like '%' + @str + '%' and (clue " + clueRep + ") and (email " + emailRep + ") and (phone " + phoneRep + ") and (adress " + adressRep + ") and id_user = @id_user";
                    else
                        command.CommandText = "select * from Table_contact where "+ toSearch + " like '%' + @str + '%' and (clue " + clueRep+") and (email "+emailRep+ ") and (phone " + phoneRep + ") and (adress " + adressRep + ") and id_user = @id_user";

                    command.Parameters.Add("@id_user", SqlDbType.Int);
                    command.Parameters["@id_user"].Value = iduser;

                    command.Parameters.Add(new SqlParameter("@str",str));

                    /*
                    command.Parameters.Add(new SqlParameter("@search", toSearch));

                    command.Parameters.Add(new SqlParameter("@clue", clueRep));
                    command.Parameters["@clue"].Value = clueRep;

                    command.Parameters.Add(new SqlParameter("@email", emailRep));

                    command.Parameters.Add(new SqlParameter("@phone", phoneRep));

                    command.Parameters.Add(new SqlParameter("@adress", adressRep));
                    */

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            contactList.Add(new Contact()
                            {
                                Id = reader.GetInt32(0),
                                FirstName = string.Format("{0}", reader[1]),
                                LastName = string.Format("{0}", reader[2]),
                                Clue = string.Format("{0}", reader[3]),
                                Email = string.Format("{0}", reader[4]),
                                Phone = string.Format("{0}", reader[5]),
                                Adress = ContactService.StringToAdress(string.Format("{0}", reader[6]))
                            });
                        }
                    }
                }

                return contactList;
            }
        }

        //retourne une list selon le firstname
        public static List<Contact> searchByFirstName(string str , int iduser)
        {
            List<Contact> contactList = new List<Contact>();
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select * from Table_contact where first_name like '%' + @fname + '%' and id_user = @id_user ";
                    command.Parameters.Add("@fname", SqlDbType.NVarChar);
                    command.Parameters["@fname"].Value = str;

                    command.Parameters.Add("@id_user", SqlDbType.Int);
                    command.Parameters["@id_user"].Value = iduser;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            contactList.Add(new Contact()
                            {
                                Id = reader.GetInt32(0),
                                FirstName = string.Format("{0}", reader[1]),
                                LastName = string.Format("{0}", reader[2]),
                                Clue = string.Format("{0}", reader[3]),
                                Email = string.Format("{0}", reader[4]),
                                Phone = string.Format("{0}", reader[5]),
                                Adress = ContactService.StringToAdress(string.Format("{0}", reader[6]))
                            });
                        }
                    }
                }

                return contactList;
            }
        }

        //retourne une liste selon le lastname
        public static List<Contact> searchByLastName(string str , int iduser)
        {
            List<Contact> contactList = new List<Contact>();
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select * from Table_contact where last_name like '%' + @lname + '%' and id_user = @id_user ";
                    command.Parameters.Add("@lname", SqlDbType.NVarChar);
                    command.Parameters["@lname"].Value = str;

                    command.Parameters.Add("@id_user", SqlDbType.Int);
                    command.Parameters["@id_user"].Value = iduser;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            contactList.Add(new Contact()
                            {
                                Id = reader.GetInt32(0),
                                FirstName = string.Format("{0}", reader[1]),
                                LastName = string.Format("{0}", reader[2]),
                                Clue = string.Format("{0}", reader[3]),
                                Email = string.Format("{0}", reader[4]),
                                Phone = string.Format("{0}", reader[5]),
                                Adress = ContactService.StringToAdress(string.Format("{0}", reader[6]))
                            });
                        }
                    }
                }

                return contactList;
            }
        }

        //retourne une liste selon lindice
        public static List<Contact> SearchByClue(string str , int iduser)
        {
            List<Contact> contactList = new List<Contact>();
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select * from Table_contact where clue like '%' + @clue + '%' and id_user = @id_user";
                    command.Parameters.Add("@clue", SqlDbType.NVarChar);
                    command.Parameters["@clue"].Value = str;

                    command.Parameters.Add("@id_user", SqlDbType.Int);
                    command.Parameters["@id_user"].Value = iduser;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            contactList.Add(new Contact() {
                                Id = reader.GetInt32(0),
                                FirstName = string.Format("{0}", reader[1]),
                                LastName = string.Format("{0}", reader[2]),
                                Clue = string.Format("{0}", reader[3]),
                                Email = string.Format("{0}", reader[4]),
                                Phone = string.Format("{0}", reader[5]),
                                Adress = ContactService.StringToAdress(string.Format("{0}", reader[6]))
                            });
                        }
                    }
                }

                return contactList;
            }
        }

        //remove un contact de la bdd
        public static void RemoveContact(Contact contactId)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "delete from Table_contact where id = @id";

                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Value = contactId.Id;

                    command.ExecuteNonQuery();
                }
            }
        }

        //update un contact dans la bdd
        public static void UpdateContact(Contact contact)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "UPDATE Table_contact SET first_name = @fname , last_name = @lname, clue = @clue , email = @email , phone = @phone , adress = @adress WHERE id= @id;  ";

                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters["@id"].Value = contact.Id;

                    command.Parameters.Add("@fname", SqlDbType.NVarChar);
                    command.Parameters["@fname"].Value = contact.FirstName.Substring(0, 1).ToUpper() + contact.FirstName.Substring(1).ToLower();

                    command.Parameters.Add("@lname", SqlDbType.NVarChar);
                    command.Parameters["@lname"].Value = contact.LastName.Substring(0, 1).ToUpper() + contact.LastName.Substring(1).ToLower();

                    if (contact.Clue == "" || contact.Clue == null)
                    {
                        command.Parameters.Add(new SqlParameter("@clue", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add("@clue", SqlDbType.NVarChar);
                        command.Parameters["@clue"].Value = contact.Clue;
                    }

                    if (contact.Email == "" || contact.Email == null)
                    {
                        command.Parameters.Add(new SqlParameter("@email", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add("@email", SqlDbType.NVarChar);
                        command.Parameters["@email"].Value = contact.Email;
                    }

                    if (contact.Phone == "" || contact.Phone == null)
                    {
                        command.Parameters.Add(new SqlParameter("@phone", DBNull.Value));
                    }
                    else
                    {
                        string firstPhonePart = "";
                        string secondPhonePart = "";
                        string thirdPhonePart = "";
                        string phone = "";

                        if (contact.Phone.Length >= 10)
                        {
                            firstPhonePart = contact.Phone.Substring(0, 3);
                            secondPhonePart = contact.Phone.Substring(3, 3);
                            thirdPhonePart = contact.Phone.Substring(6, 4);
                            phone = firstPhonePart + "-" + secondPhonePart + "-" + thirdPhonePart;
                        }

                        command.Parameters.Add("@phone", SqlDbType.NVarChar);
                        command.Parameters["@phone"].Value = phone;
                    }



                    if (contact.Adress.RoadName != "" && contact.Adress.NoHouse != null && contact.Adress.City != "" && contact.Adress.Country != "" && contact.Adress.State != "")
                    {
                        command.Parameters.Add("@adress", SqlDbType.NVarChar);
                        command.Parameters["@adress"].Value = contact.Adress.NoHouse + "/" + contact.Adress.RoadName + "/" + contact.Adress.City + "/" + contact.Adress.Country + "/" + contact.Adress.State;
                    }
                    else
                    {
                        command.Parameters.Add(new SqlParameter("@adress", DBNull.Value));
                    }

                    command.ExecuteNonQuery();
                }
            }
        }

        //ajoute un contact dans la bdd
        public static void AddContact(Contact contact)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "insert into Table_contact(first_name,last_name,clue,email,phone,adress,id_user) values(@fname,@lname,@clue,@email,@phone,@adress,@id_user)";

                    command.Parameters.Add("@fname", SqlDbType.NVarChar);
                    command.Parameters["@fname"].Value = contact.FirstName.Substring(0, 1).ToUpper() + contact.FirstName.Substring(1).ToLower();

                    command.Parameters.Add("@lname", SqlDbType.NVarChar);
                    command.Parameters["@lname"].Value = contact.LastName.Substring(0, 1).ToUpper() + contact.LastName.Substring(1).ToLower();

                    if(contact.Clue == "" || contact.Clue == null)
                    {
                        command.Parameters.Add(new SqlParameter("@clue", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add("@clue", SqlDbType.NVarChar);
                        command.Parameters["@clue"].Value = contact.Clue;
                    }

                    if(contact.Email == "" || contact.Email == null)
                    {
                        command.Parameters.Add(new SqlParameter("@email", DBNull.Value));
                    }
                    else
                    {
                        command.Parameters.Add("@email", SqlDbType.NVarChar);
                        command.Parameters["@email"].Value = contact.Email;
                    }

                    if(contact.Phone == "" || contact.Phone == null)
                    {
                        command.Parameters.Add(new SqlParameter("@phone", DBNull.Value));
                    }
                    else
                    {
                        string firstPhonePart = "";
                        string secondPhonePart = "";
                        string thirdPhonePart = "";
                        string phone = "";

                        if (contact.Phone.Length >= 10)
                        {
                            firstPhonePart = contact.Phone.Substring(0, 3);
                            secondPhonePart = contact.Phone.Substring(3, 3);
                            thirdPhonePart = contact.Phone.Substring(6, 4);
                            phone = firstPhonePart + "-" + secondPhonePart + "-" + thirdPhonePart;
                        }

                        command.Parameters.Add("@phone", SqlDbType.NVarChar);
                        command.Parameters["@phone"].Value = phone;
                    }

                   

                    if (contact.Adress.RoadName != "" && contact.Adress.NoHouse != null &&contact.Adress.City != "" && contact.Adress.Country != "" && contact.Adress.State != "")
                    {
                        command.Parameters.Add("@adress", SqlDbType.NVarChar);
                        command.Parameters["@adress"].Value = contact.Adress.NoHouse + "/" + contact.Adress.RoadName + "/" + contact.Adress.City + "/" + contact.Adress.Country + "/" + contact.Adress.State;
                    }                      
                    else
                    {
                        command.Parameters.Add(new SqlParameter("@adress", DBNull.Value));
                    }
                        


                    command.Parameters.Add("@id_user", SqlDbType.Int);
                    command.Parameters["@id_user"].Value = contact.Id_user;

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
