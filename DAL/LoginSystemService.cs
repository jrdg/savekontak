﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public static class LoginSystemService
    {
        //test lsi le login existe dans la bdd
        public static bool Connect(LoginSystem loginsystem)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select username , password from account where username = @username and password = @password";

                    command.Parameters.Add("@username", SqlDbType.NVarChar);
                    command.Parameters["@username"].Value = loginsystem.Username;

                    command.Parameters.Add("@password", SqlDbType.NVarChar);
                    command.Parameters["@password"].Value = loginsystem.Password;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            return true;
                        else
                            return false;
                    }
                }
            }
        }
    }
}
