﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL
{
    public class Session
    {
        private Session session;
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool? Sexe { get; set; } //true = men / false = female / null = unknown

        //instance de session quon va devoir donner les info nous meme
        public Session()
        {

        }

        //instance de session qui va lancer une requete au serveur selon le nom et renvoyez les info du user.
        public Session(string username)
        {
            SqlConnection conn;
            string myConn = "Data Source=DESKTOP-0IQOSNB\\SQLEXPRESS; Initial Catalog=saveKontak; Integrated security=True";

            using (conn = new SqlConnection(myConn))
            {
                conn.Open();

                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = "select * from account where username = @username";
                    command.Parameters.Add("@username", SqlDbType.NVarChar);
                    command.Parameters["@username"].Value = username;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            Id = reader.GetInt32(0);
                            Username = string.Format("{0}", reader[1]);
                            Password = string.Format("{0}", reader[2]);
                            FirstName = string.Format("{0}", reader[3]);
                            LastName = string.Format("{0}", reader[4]);
                            Email = string.Format("{0}", reader[5]);
                            Phone = string.Format("{0}", reader[6]);

                            if (string.Format("{0}", reader[7]) == "Male")
                                Sexe = true;
                            else if (string.Format("{0}", reader[7]) == "Female")
                                Sexe = false;
                            else
                                Sexe = null;                            
                        }
                    }
                }
            }
        }

        //instance de session a partir dun autre instance
        public Session(Session session)
        {
            Id = session.Id;
            Username = session.Username;
            Password = session.Password;
            FirstName = session.FirstName;
            LastName = session.LastName;
            Email = session.Email;
            Phone = session.Phone;
        }
    }
}
