﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace BLL
{
    public static class Validation
    {
        //test les champ dun contact
        public static bool ContactValidation(Contact contact)
        {
            if (
                (IsMail(contact.Email) || contact.Email == "") &&
                Regex.IsMatch(contact.FirstName, @"^[a-zA-Z--]+$") &&
                Regex.IsMatch(contact.LastName, @"^[a-zA-Z]+$") &&
                (Regex.IsMatch(contact.Phone, @"^[0-9]+$") || contact.Phone == "") &&
                (Regex.IsMatch(contact.Clue, @"^[ a-zA-Z]+$") || contact.Clue == "") &&
                (Regex.IsMatch(string.Format("{0}", contact.Adress.NoHouse), @"^[0-9]+$") || contact.Adress.NoHouse == null) &&
                (Regex.IsMatch(contact.Adress.RoadName, @"^[ a-zA-Z--]+$") || contact.Adress.RoadName == "") &&
                (Regex.IsMatch(contact.Adress.State, @"^[a-zA-Z--]+$") || contact.Adress.State == "") &&
                (Regex.IsMatch(contact.Adress.Country, @"^[a-zA-Z--]+$") || contact.Adress.Country == "") &&
                (Regex.IsMatch(contact.Adress.City, @"^[a-zA-Z--]+$") || contact.Adress.City == "")
                )
                return true;
            else
                return false;
        }

        //test si le mail es bien un mail
        public static bool IsMail(string emailaddress)
        {
            if (emailaddress != "")
            {
                try
                {
                    MailAddress m = new MailAddress(emailaddress);

                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
            else
                return false;
        }
    }
}
