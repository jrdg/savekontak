﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using DAL;

namespace BLL
{
    public static class AccountManager
    {
        //appel la fonction qui ajoute un compte dans le dal
        public static bool AddAccount(Account account)
        {
            if (AccountService.AddAccount(account))
                return true;
            else
                return false;
        }

        //appel la fonction qui remove un compte dans le dal
        public static bool RemoveAccount(Account account)
        {
            AccountService.RemoveAccount(account);

            return true;
        }

        //appel la fonction qui update un compte dans le dal
        public static bool UpdateAccount(Account account , string type)
        {
            AccountService.UpdateAccount(account,type);

            return true;
        }
    }
}
