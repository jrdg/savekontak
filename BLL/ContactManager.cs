﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using DAL;

namespace BLL
{

    public static class ContactManager
    {
        //appel la fonction recherche un contact par son id dans le dal
        public static Contact getContactById(int id)
        {
            return ContactService.getContactById(id);
        }

        //appel la fonction qui ajoute un contact dans le dal
        public static bool AddContact(Contact contact)
        {
            if (Validation.ContactValidation(contact))
           {
                ContactService.AddContact(contact);
                return true;
           }
           else
                return false;
        }

        //appel la fonction qui remove un contact dans le dal
        public static void RemoveContact(Contact contact)
        {
            ContactService.RemoveContact(contact);
        }

        //appel la fonction qui update un contact dans le dal
        public static bool UpdateContact(Contact contact)
        {
            if (Validation.ContactValidation(contact))
            {
                ContactService.UpdateContact(contact);
                return true;
            }
            else
                return false;             
        }
        
        //appel la fonction qui retourne une liste de contact selon son prenom dans le dal
        public static List<Contact> searchByFirstName(string str,int iduser)
        {
           return ContactService.searchByFirstName(str,iduser);
        }

        //appel la fonction qui retourne une liste de contact selon son nom dans le dal
        public static List<Contact> serchByLastname(string str,int iduser)
        {
            return ContactService.searchByLastName(str,iduser);
        }

        //appel la fonction qui retourne une liste de contact selon son indice dans le dal
        public static List<Contact> searchByClue(string str,int iduser)
        {
            return ContactService.SearchByClue(str,iduser);
        }

        //appel la fonction qui retourne une liste de contact selon les critere dans le dal
        public static List<Contact> searchadvancedSetting(string str, searchBy sb, bool? cluenull, bool? emailnull, bool? phonenull, bool? adressnull,int iduser)
        {
            return ContactService.searchadvancedSetting(str, sb, cluenull, emailnull, phonenull, adressnull,iduser);
        }

        //appel la fonction qui retourne tous les contact dans le dal
        public static List<Contact> getAllContact(Account accountId)
        {
            return ContactService.getAllContact(accountId);
        }
    }
}
