﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
using BLL;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace Vue
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public RegisterWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ResizeMode = ResizeMode.NoResize;
            this.button_backlogin.Click += Button_backlogin_Click;
            this.button_register.Click += Button_register_Click;
        }

        //valid les input et enregistre le nouvelle account dans la bdd
        private void Button_register_Click(object sender, RoutedEventArgs e)
        {
            string errMsg = "";

            bool usernameValid = false;
            bool passwordvalid = false;
            bool firstNameValid = false;
            bool lastNameValid = false;
            bool sexeValid = false;
            bool phoneValid = false;
            bool emailValid = false;
            bool? sexe = true;

            //validation username
            if (this.textbox_username.Text != "")
            {
                if (Regex.IsMatch(this.textbox_username.Text, @"^[a-z0-9]+$") && this.textbox_username.Text.Length < 20)
                    usernameValid = true;
                else
                    errMsg += "USERNAME ERROR : Username only accept letter tolower and number \n";
            }
            else
                errMsg += "USERNAME ERROR : You must choose a username \n";

            //validation password
            if (this.textbox_password.Password != "")
            {
                if (this.textbox_password.Password == this.textbox_password_confirmation.Password)
                    passwordvalid = true;
                else
                    errMsg += "PASSWORD ERROR : Password and password** must be the same \n";
            }
            else
                errMsg += "PASSWORD ERROR : You must choose a password \n";

            //validation firstName
            if (this.textbox_fname.Text != "")
            {
                if (Regex.IsMatch(this.textbox_fname.Text, @"^[a-zA-Z--]+$") && this.textbox_fname.Text.Length < 20)
                    firstNameValid = true;
                else
                    errMsg += "FIRST NAME ERROR : First name only accept letter and - \n";
            }
            else
                errMsg += "FIRST NAME ERROR : You must have a first name \n";

            //validation lastname
            if (this.textbox_lname.Text != "")
            {
                if (Regex.IsMatch(this.textbox_lname.Text, @"^[a-zA-Z]+$") && this.textbox_lname.Text.Length < 20)
                    lastNameValid = true;
                else
                    errMsg += "LAST NAME ERROR : Last name only accept letter and - \n";
            }
            else
                errMsg += "LAST NAME ERROR : You must have a last name \n";

            //validation sexe
            if (this.radiobutton_male.IsChecked == true || this.radiobutton_female.IsChecked == true || this.radiobutton_unknown.IsChecked == true)
                sexeValid = true;
            else
                errMsg += "GENDER ERROR : You need to specify your gender \n";

            //validation phone
            if (this.textbox_phone.Text != "")
            {
                if (Regex.IsMatch(this.textbox_phone.Text, @"^[0-9]+$") && this.textbox_phone.Text.Length == 10)
                    phoneValid = true;
                else
                    errMsg += "PHONE ERROR : Phone only accept number and need to have 10 number \n";
            }
            else
                errMsg += "PHONE ERROR : You must have a phone number \n";

            //validation email
            if (this.textbox_email.Text != "")
            {
                if (BLL.Validation.IsMail(this.textbox_email.Text))
                    emailValid = true;
                else
                    errMsg += "EMAIL ERROR : Your email must have this format : exemple@email.com \n";
            }
            else
                errMsg += "EMAIL ERROR : You must have an email \n";

            if (
                usernameValid &&
                passwordvalid &&
                firstNameValid &&
                lastNameValid &&
                sexeValid &&
                phoneValid &&
                emailValid
                )
            {
                if (this.radiobutton_male.IsChecked == true)
                    sexe = true;
                else if (this.radiobutton_female.IsChecked == true)
                    sexe = false;
                else if(this.radiobutton_unknown.IsChecked == true)
                    sexe = null;

                if (AccountManager.AddAccount(new Account()
                {
                    Username = this.textbox_username.Text.Trim(),
                    Password = this.textbox_password.Password.Trim(),
                    FirstName = this.textbox_fname.Text.Trim(),
                    LastName = this.textbox_lname.Text.Trim(),
                    Sexe = sexe,
                    Phone = this.textbox_phone.Text.Trim(),
                    Email = this.textbox_email.Text.Trim()
                    ,
                }))
                {
                    MainWindow mw = new MainWindow();
                    MessageBox.Show("You can now login with your username and password");
                    this.Close();
                    mw.ShowDialog();
                }
                else
                    MessageBox.Show("USERNAME ERROR : Username already taken");
            }
            else
                MessageBox.Show(errMsg);
        }

        //ramene lutilisateur au login
        private void Button_backlogin_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.ShowDialog();
        }

        public bool IsMail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
