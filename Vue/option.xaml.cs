﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
using BLL;
using DAL;
using System.Security.Cryptography;

namespace Vue
{
    /// <summary>
    /// Interaction logic for option.xaml
    /// </summary>
    public partial class option : Window
    {
        public Session Session { get; set; }

        public option()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ResizeMode = ResizeMode.CanMinimize;
            this.saveemail.Click += Saveemail_Click;
            this.savepassword.Click += Savepassword_Click;
        }

        private void Savepassword_Click(object sender, RoutedEventArgs e)
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            bool current = false;
            bool newpw = false;
            string currentpw = BitConverter.ToString(md5.ComputeHash(utf8.GetBytes(this.currentpassword.Password))).Trim();

            if (currentpw == Session.Password.Trim())
            {
                this.errmsg.Text = "";
                current = true;
            }               
            else
                this.errmsg.Text = "Bad current password";

            if (this.newpassword.Password == this.newpasswordconfirmation.Password && (this.newpassword.Password != ""))
            {
                this.errmsg.Text = "";
                newpw = true;
            }
            else
                this.errmsg.Text = "password mismatch";
                

            if(current && newpw)
            {
                this.errmsg.Text = "Password have been changed";
                AccountManager.UpdateAccount(new Account() { Password = this.newpassword.Password.Trim(), Id = Session.Id },"pw");
            }
        }

        private void Saveemail_Click(object sender, RoutedEventArgs e)
        {
            if (this.email.Text != "")
            {
                this.errmsg.Text = "";

                if(BLL.Validation.IsMail(this.email.Text))
                {
                    UTF8Encoding utf8 = new UTF8Encoding();
                    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

                    this.errmsg.Text = "";
                    AccountManager.UpdateAccount(new Account() {Email = this.email.Text.Trim() , Id = Session.Id }, "email" );

                    this.errmsg.Text = "Email have been changed";

                }
                else
                    this.errmsg.Text = "email not valid";
            }
            else
                this.errmsg.Text = "You need an email!";
        }
    }
}
