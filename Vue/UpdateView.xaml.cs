﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
using BLL;
using DAL;
using System.Text.RegularExpressions;

namespace Vue
{
    /// <summary>
    /// Interaction logic for UpdateView.xaml
    /// </summary>
    public partial class UpdateView : Window
    {
        public int IdContact { get; set; }
        Contact contact;

        public UpdateView()
        {
            this.Loaded += UpdateView_Loaded;
            this.Title = "Update contact";
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ResizeMode = ResizeMode.CanMinimize;
        }

        //le load event
        private void UpdateView_Loaded(object sender, RoutedEventArgs e)
        {
            this.contact = ContactManager.getContactById(IdContact);
            this.textbox_fname.Text = contact.FirstName;
            this.textbox_lname.Text = contact.LastName;
            this.textbox_clue.Text = contact.Clue;
            if(contact.Phone == "" || contact.Phone == null)
            {
                this.textbox_phone.Text = "";
            }
            else
            {
                string[] mytab = contact.Phone.Split('-');
                this.textbox_phone.Text = mytab[0] + mytab[1] + mytab[2];
            }
            this.textbox_email.Text = contact.Email;
            this.textbox_nohouse.Text = string.Format("{0}",contact.Adress.NoHouse);
            this.textbox_street.Text = contact.Adress.RoadName;
            this.textbox_city.Text = contact.Adress.City;
            this.textbox_country.Text = contact.Adress.Country;
            this.textbox_state.Text = contact.Adress.State;
            this.button_add.Click += Button_add_Click;
            this.button_reset.Click += Button_reset_Click;
        }

        //reset tous les donner du membre a quesequil etais au debut
        private void Button_reset_Click(object sender, RoutedEventArgs e)
        {
            this.textbox_city.Text = contact.Adress.City;
            this.textbox_clue.Text = contact.Clue;
            this.textbox_country.Text = contact.Adress.Country;
            this.textbox_email.Text = contact.Email;
            this.textbox_fname.Text = contact.FirstName;
            this.textbox_lname.Text = contact.LastName;
            this.textbox_nohouse.Text = string.Format("{0}",contact.Adress.NoHouse);
            if (contact.Phone != "" && contact.Phone != null)
            {
                string[] mytab = contact.Phone.Split('-');
                this.textbox_phone.Text = mytab[0] + mytab[1] + mytab[2];
            }
            else
                this.textbox_phone.Text = "";          
            this.textbox_state.Text = contact.Adress.State;
            this.textbox_street.Text = contact.Adress.RoadName;
            this.err_city.Content = "";
            this.err_clue.Content = "";
            this.err_country.Content = "";
            this.err_email.Content = "";
            this.err_fname.Content = "";
            this.err_lname.Content = "";
            this.err_nohouse.Content = "";
            this.err_phone.Content = "";
            this.err_state.Content = "";
            this.err_street.Content = "";
        }

        //valid les input et update le contact
        private void Button_add_Click(object sender, RoutedEventArgs e)
        {
            bool fnameValid = false;
            bool lnameValid = false;
            bool clueValid = false;
            bool emailValid = false;
            bool phoneValid = false;
            bool noHouseValid = false;
            bool streetValid = false;
            bool cityValid = false;
            bool countryValid = false;
            bool stateValid = false;

            //validation firstname
            if (this.textbox_fname.Text != "")
            {
                this.err_fname.Content = "";

                if (Regex.IsMatch(this.textbox_fname.Text, @"^[a-zA-Z--]+$") && this.textbox_fname.Text.Length < 25)
                {
                    this.err_fname.Content = "";
                    fnameValid = true;
                }
                else
                    this.err_fname.Content = "25 letter or less and no numeric value";
            }
            else
                this.err_fname.Content = "You need to fill this input";

            //validation lastname
            if (this.textbox_lname.Text != "")
            {
                this.err_lname.Content = "";

                if (Regex.IsMatch(this.textbox_lname.Text, @"^[a-zA-Z--]+$") && this.textbox_lname.Text.Length < 25)
                {
                    this.err_lname.Content = "";
                    lnameValid = true;
                }
                else
                    this.err_lname.Content = "25 letter or less and no numeric value";
            }
            else
                this.err_lname.Content = "You need to fill this input";

            //validation clue
            if (this.textbox_clue.Text != "")
            {
                this.err_clue.Content = "";

                if (Regex.IsMatch(this.textbox_clue.Text, @"^[ a-zA-Z]+$") && this.textbox_clue.Text.Length < 25)
                {
                    this.err_clue.Content = "";
                    clueValid = true;
                }
                else
                    this.err_clue.Content = "25 letter or less and no numeric value";
            }
            else
            {
                this.err_clue.Content = "";
                clueValid = true;
            }

            //validation email
            if (this.textbox_email.Text != "")
            {
                if (BLL.Validation.IsMail(this.textbox_email.Text))
                {
                    this.err_email.Content = "";
                    emailValid = true;
                }
                else
                    this.err_email.Content = "Email not valid";
            }
            else
            {
                this.err_email.Content = "";
                emailValid = true;
            }

            //validation phone
            if (this.textbox_phone.Text != "")
            {
                if (Regex.IsMatch(this.textbox_phone.Text, @"^[0-9]+$") && this.textbox_phone.Text.Length == 10)
                {
                    this.err_phone.Content = "";
                    phoneValid = true;
                }
                else
                    this.err_phone.Content = "Respect this format : 5556667777";
            }
            else
            {
                this.err_phone.Content = "";
                phoneValid = true;
            }

            //validation adresse
            if (this.textbox_nohouse.Text != "" || this.textbox_street.Text != "" || this.textbox_state.Text != "" || this.textbox_country.Text != "" || this.textbox_city.Text != "")
            {
                //validation no house
                if (this.textbox_nohouse.Text != "")
                {
                    this.err_nohouse.Content = "";

                    if (Regex.IsMatch(this.textbox_nohouse.Text, @"^[0-9]+$"))
                    {
                        this.err_nohouse.Content = "";
                        noHouseValid = true;
                    }
                    else
                        this.err_nohouse.Content = "Need to be numeric";
                }
                else
                    this.err_nohouse.Content = "Need to be fill";


                //validation rue
                if (this.textbox_street.Text != "")
                {
                    this.err_street.Content = "";

                    if (Regex.IsMatch(this.textbox_street.Text, @"^[ a-zA-Z--]+$"))
                    {
                        this.err_street.Content = "";
                        streetValid = true;
                    }
                    else
                        this.err_street.Content = "No numeric value";
                }
                else
                    this.err_street.Content = "Need to be fill";


                //validation country
                if (this.textbox_country.Text != "")
                {
                    this.err_country.Content = "";

                    if (Regex.IsMatch(this.textbox_country.Text, @"^[a-zA-Z--]+$"))
                    {
                        this.err_country.Content = "";
                        countryValid = true;
                    }
                    else
                        this.err_country.Content = "No numeric value";
                }
                else
                    this.err_country.Content = "Need to be fill";

                //validation city
                if (this.textbox_city.Text != "")
                {
                    this.err_city.Content = "";

                    if (Regex.IsMatch(this.textbox_city.Text, @"^[a-zA-Z--]+$"))
                    {
                        this.err_city.Content = "";
                        cityValid = true;
                    }
                    else
                        this.err_city.Content = "No numeric value";
                }
                else
                    this.err_city.Content = "Need to be fill";


                //validation state
                if (this.textbox_state.Text != "")
                {
                    this.err_state.Content = "";

                    if (Regex.IsMatch(this.textbox_state.Text, @"^[a-zA-Z--]+$"))
                    {
                        this.err_state.Content = "";
                        stateValid = true;
                    }
                    else
                        this.err_state.Content = "No numeric value";
                }
                else
                    this.err_state.Content = "Need to be fill";

            }
            else
            {
                cityValid = true;
                countryValid = true;
                stateValid = true;
                streetValid = true;
                noHouseValid = true;
            }

            if (fnameValid && lnameValid && phoneValid && emailValid && clueValid &&
                streetValid && countryValid && cityValid && noHouseValid && stateValid)
            {

                int? noHouse = 0;
                if (this.textbox_nohouse.Text != "")
                    noHouse = int.Parse(this.textbox_nohouse.Text);
                else
                    noHouse = null;

           

                if(MessageBox.Show("Do you really want to update this contact","Update confirmation",MessageBoxButton.YesNo,MessageBoxImage.Question)== MessageBoxResult.Yes)
                {
                    ContactManager.UpdateContact(new Contact()
                    {
                        Id = IdContact,
                        FirstName = this.textbox_fname.Text,
                        LastName = this.textbox_lname.Text,
                        Clue = this.textbox_clue.Text,
                        Email = this.textbox_email.Text,
                        Phone = this.textbox_phone.Text,
                        Adress = new Adress()
                        {
                            City = this.textbox_city.Text,
                            Country = this.textbox_country.Text,
                            NoHouse = noHouse,
                            RoadName = this.textbox_street.Text,
                            State = this.textbox_state.Text
                        }
                    });
                    this.Close();
                }
            }
        }
    }
}
