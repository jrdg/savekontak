﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BLL;
using Model;
using DAL;

namespace Vue
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ResizeMode = ResizeMode.NoResize;
            this.button_register.Click += Button_register_Click;
            this.button_login.Click += Button_login_Click; 
        }

        //test si les valeur entrer par le user son bien un account valid
        private void Button_login_Click(object sender, RoutedEventArgs e)
        {
            if (this.textbox_username.Text != "" && this.textbox_password.Password != "")
            {
                if (LoginSystemManager.Connect(
                    new LoginSystem(
                        new Account() { Username = this.textbox_username.Text, Password = this.textbox_password.Password })))
                {
                    User userWindow = new User() { session = new Session(this.textbox_username.Text) };
                    this.Close();
                    userWindow.ShowDialog();
                }
                else
                    MessageBox.Show("Bad username or password!");
            }
            else
                MessageBox.Show("You need to specify both input to connect");
        }

        //amene lutilisateur sur la window pour senregistrer
        private void Button_register_Click(object sender, RoutedEventArgs e)
        {
            RegisterWindow rw = new RegisterWindow();
            this.Close();
            rw.ShowDialog();
        }
    }
}
