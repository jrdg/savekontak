﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using DAL;
using Model;
using BLL;
using System.Windows.Controls.Primitives;
using System;

namespace Vue
{
    /// <summary>
    /// Interaction logic for User.xaml
    /// </summary>
    public partial class User : Window
    {
        public Session session { get; set; }
        List<Contact> contactList;

        public User()
        {
            this.Loaded += User_Loaded;
            InitializeComponent();
            this.disconnect_user.Click += Disconnect_user_Click;
            this.ResizeMode = ResizeMode.CanMinimize;
            this.button_add.Click += Button_add_Click;
            this.button_remove.Click += Button_remove_Click;
            this.button_save.Click += Button_save_Click;
            List<string> slist = new List<string>();
            slist.Add("Firstname");
            slist.Add("Lastname");
            slist.Add("clue");
            slist.Add("View All");
            this.combo.ItemsSource = slist;
            this.search.Click += Search_Click;
            this.checkbox_active_advanced_setting.Click += Checkbox_active_advanced_setting_Click;
            this.checkbox_adress.IsEnabled = false;
            this.checkbox_clue.IsEnabled = false;
            this.checkbox_email.IsEnabled = false;
            this.checkbox_phone.IsEnabled = false;
            this.combo.SelectedIndex = 3;
            this.combo.SelectionChanged += Combo_SelectionChanged;
            this.checkbox_active_advanced_setting.IsEnabled = false;
            this.searchbox.IsEnabled = false;
            this.user_option.Click += User_option_Click;
        }

        private void User_option_Click(object sender, RoutedEventArgs e)
        {
            option op = new option() { Session = this.session };
            op.ShowDialog();
        }

        //fais ens orte dactiver et de desactiver les critere de recherche et la search box quand on es sur viewall
        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.combo.SelectedIndex == 3)
            {
                this.checkbox_active_advanced_setting.IsEnabled = false;
                this.checkbox_active_advanced_setting.IsChecked = false;
                this.searchbox.Text = "";
                this.searchbox.IsEnabled = false;
                this.checkbox_adress.IsChecked = false;
                this.checkbox_clue.IsChecked = false;
                this.checkbox_email.IsChecked = false;
                this.checkbox_phone.IsChecked = false;
                this.checkbox_adress.IsEnabled = false;
                this.checkbox_clue.IsEnabled = false;
                this.checkbox_email.IsEnabled = false;
                this.checkbox_phone.IsEnabled = false;
            }
            else
            {
                this.checkbox_active_advanced_setting.IsEnabled = true;
                this.searchbox.IsEnabled = true;
            }
        }

        //active les multicritere kan la box active es cocher
        private void Checkbox_active_advanced_setting_Click(object sender, RoutedEventArgs e)
        {
            if (this.checkbox_active_advanced_setting.IsChecked == true)
            {
                this.checkbox_adress.IsEnabled = true;
                this.checkbox_clue.IsEnabled = true;
                this.checkbox_email.IsEnabled = true;
                this.checkbox_phone.IsEnabled = true;
            }
            else
            {
                this.checkbox_adress.IsChecked = false;
                this.checkbox_clue.IsChecked = false;
                this.checkbox_email.IsChecked = false;
                this.checkbox_phone.IsChecked = false;
                this.checkbox_adress.IsEnabled = false;
                this.checkbox_clue.IsEnabled = false;
                this.checkbox_email.IsEnabled = false;
                this.checkbox_phone.IsEnabled = false;
            }
        }

        //fais la recherche selon les critere donne
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            bool? clue = null;
            bool? email = null;
            bool? adress = null;
            bool? phone = null;

            if (this.checkbox_active_advanced_setting.IsChecked == true)
            {
                if (this.checkbox_adress.IsChecked == true)
                    adress = false;
                else if (this.checkbox_adress.IsChecked == false)
                    adress = null;
                if (this.checkbox_clue.IsChecked == true)
                    clue = false;
                else if (this.checkbox_clue.IsChecked == false)
                    clue = null;
                if (this.checkbox_email.IsChecked == true)
                    email = false;
                else if (this.checkbox_email.IsChecked == false)
                    email = null;
                if (this.checkbox_phone.IsChecked == true)
                    phone = false;
                else if (this.checkbox_phone.IsChecked == false)
                    phone = null;
            }


            if (int.Parse(this.combo.SelectedIndex.ToString()) == 0)
            {
                if(this.checkbox_active_advanced_setting.IsChecked == true)
                {
                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.searchadvancedSetting(this.searchbox.Text, Model.searchBy.FirstName, clue, email, phone, adress,session.Id);
                }
                else
                {
                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.searchByFirstName(this.searchbox.Text,session.Id);
                }

            }
            else if(int.Parse(this.combo.SelectedIndex.ToString()) == 1)
            {
                if (this.checkbox_active_advanced_setting.IsChecked == true)
                {
                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.searchadvancedSetting(this.searchbox.Text, Model.searchBy.Lastname, clue, email, phone, adress,session.Id);
                }
                else
                {
                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.serchByLastname(this.searchbox.Text,session.Id);
                }
            }
            else if(int.Parse(this.combo.SelectedIndex.ToString()) == 2)
            {
                if (this.checkbox_active_advanced_setting.IsChecked == true)
                {
                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.searchadvancedSetting(this.searchbox.Text, Model.searchBy.clue, clue, email, phone, adress,session.Id);
                }
                else
                {
                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.searchByClue(this.searchbox.Text,session.Id);
                }
            }   
            else if(int.Parse(this.combo.SelectedIndex.ToString()) == 3)
            {
                this.contactGrid.ItemsSource = null;
                this.contactGrid.ItemsSource = ContactManager.getAllContact(new Account() { Id = session.Id });
            }           
        }

        //amene lutilisateur a la window update
        private void Button_save_Click(object sender, RoutedEventArgs e)
        {
            if (this.contactGrid.SelectedItems.Count == 1)
            {
                UpdateView uv = new UpdateView() { IdContact = int.Parse(this.contactGrid.SelectedValue.ToString()) };
                uv.ShowDialog();
                this.contactGrid.ItemsSource = null;
                this.contactGrid.ItemsSource = ContactManager.getAllContact(new Account() { Id = session.Id });
            }
            else
                MessageBox.Show("You need to select a contact for use the update option");

        }

        //remove la selection de contact
        private void Button_remove_Click(object sender, RoutedEventArgs e)
        {
            if(this.contactGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Are you sure you want to remove all the selected contact because its final you cant back", "Remove confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    foreach (var item in this.contactGrid.SelectedItems)
                    {
                        ContactManager.RemoveContact(new Contact() { Id = Convert.ToInt32(item.ToString()) });
                    }

                    this.contactGrid.ItemsSource = null;
                    this.contactGrid.ItemsSource = ContactManager.getAllContact(new Account() { Id = session.Id });
                }
            }
            else
                MessageBox.Show("You need to select a contact for use the update option");
        }

        //amene lutilisateur a la window dajout de contact
        private void Button_add_Click(object sender, RoutedEventArgs e)
        {
            AddContactWindow aw = new AddContactWindow() { Session = session };
            aw.ShowDialog();
            contactGrid.ItemsSource = null;
            contactGrid.ItemsSource = ContactManager.getAllContact(new Account() { Id = session.Id });

        }

        //this.loaded
        private void User_Loaded(object sender, RoutedEventArgs e)
        {

            this.label_loggedIn.Content += " "+this.session.Username;

            contactList = ContactManager.getAllContact(new Account() { Id = session.Id });

            this.contactGrid.ItemsSource = contactList;
            this.contactGrid.IsReadOnly = true;
        }

        //si lutilisateur appui sur disconnect
        private void Disconnect_user_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to disconnect ?", "Disconnect", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MainWindow mw = new MainWindow();
                this.Close();
                mw.ShowDialog();
            }
        }
    }
}
