﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Adress
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string RoadName { get; set; }
        public int? NoHouse { get; set; }

        public override string ToString()
        {
            if (City == null || State == null || Country == null || RoadName == null || NoHouse == null)
                return "";
            else
                return  NoHouse + "/" + RoadName + "/" + City + "/" + Country + "/" + State;
        }
    }
}
