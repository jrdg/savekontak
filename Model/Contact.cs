﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Contact
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Clue { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Adress Adress { get; set; }
        public int Id_user { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", Id);
        }
    }
}
