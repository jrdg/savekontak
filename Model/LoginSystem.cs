﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class LoginSystem
    {
        private Account account;

        public LoginSystem(Account account)
        {
            this.account = account;
        }

        public string Username
        {
            get
            {
                return this.account.Username;
            }
        }

        public string Password
        {
            get
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

                return BitConverter.ToString(md5.ComputeHash(utf8.GetBytes(this.account.Password)));
            }
        }
    }
}
